package grid

import (
	"fmt"
	"testing"

	"github.com/brettbates/in"
)

type sampleGrid struct {
	str     string
	xSize   int
	ySize   int
	sSize   int
	grid    IntGrid
	scanned *[][]int
	summed  *[]int
}

var sampleHScans = [][][]int{
	{{1, 2}, {2, 3}, {3, 4}, {5, 6}, {6, 7}, {7, 8}},
	{{1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}},
	{{1, 2, 3}, {2, 3, 4}, {5, 6, 7}, {6, 7, 8}, {9, 10, 11}, {10, 11, 12}, {13, 14, 15}, {14, 15, 16}},
}

var sampleVScans = [][][]int{
	{{1, 5}, {2, 6}, {3, 7}, {4, 8}},
	{{1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}},
	{{1, 5, 9}, {5, 9, 13}, {2, 6, 10}, {6, 10, 14}, {3, 7, 11}, {7, 11, 15}, {4, 8, 12}, {8, 12, 16}},
}

var sampleDScans = [][][]int{
	{{1, 6}, {2, 7}, {3, 8}, {4, 7}, {3, 6}, {2, 5}},
	{{1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}},
	{{1, 6, 11}, {2, 7, 12}, {4, 7, 10}, {3, 6, 9}, {5, 10, 15}, {6, 11, 16}, {8, 11, 14}, {7, 10, 13}},
}

var sampleSums = [][]int{
	{3, 5, 7, 11, 13, 15},
	{1, 2, 3, 4, 5, 6, 7, 8},
	{6, 9, 18, 21, 30, 33, 42, 45},
}

//TODO: refactor these 3 out so not repeating the same data for no gain
var sampleHGrids = []sampleGrid{
	{"1 2 3 4 5 6 7 8", 4, 2, 2, IntGrid{{1, 2, 3, 4}, {5, 6, 7, 8}}, &sampleHScans[0], &sampleSums[0]},
	{"1 2 3 4 5 6 7 8", 2, 4, 1, IntGrid{{1, 2}, {3, 4}, {5, 6}, {7, 8}}, &sampleHScans[1], &sampleSums[1]},
	{"01 02 3 4 5 06 07 08 09 010 11 12 13 14 15 16", 4, 4, 3,
		IntGrid{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}, &sampleHScans[2], &sampleSums[2]},
}

var sampleVGrids = []sampleGrid{
	{"1 2 3 4 5 6 7 8", 4, 2, 2, IntGrid{{1, 2, 3, 4}, {5, 6, 7, 8}}, &sampleVScans[0], &sampleSums[0]},
	{"1 2 3 4 5 6 7 8", 2, 4, 1, IntGrid{{1, 2}, {3, 4}, {5, 6}, {7, 8}}, &sampleVScans[1], &sampleSums[1]},
	{"01 02 3 4 5 06 07 08 09 010 11 12 13 14 15 16", 4, 4, 3,
		IntGrid{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}, &sampleVScans[2], &sampleSums[2]},
}

var sampleDGrids = []sampleGrid{
	{"1 2 3 4 5 6 7 8", 4, 2, 2, IntGrid{{1, 2, 3, 4}, {5, 6, 7, 8}}, &sampleDScans[0], &sampleSums[0]},
	{"1 2 3 4 5 6 7 8", 2, 4, 1, IntGrid{{1, 2}, {3, 4}, {5, 6}, {7, 8}}, &sampleDScans[1], &sampleSums[1]},
	{"01 02 3 4 5 06 07 08 09 010 11 12 13 14 15 16", 4, 4, 3,
		IntGrid{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}}, &sampleDScans[2], &sampleSums[2]},
}

func TestStringToIntSlice(t *testing.T) {
	for _, sg := range sampleHGrids {
		ng := NewIntGrid(sg.str, sg.xSize, sg.ySize)
		if fmt.Sprintf("%v", ng) != fmt.Sprintf("%v", sg.grid) {
			t.Error("Expected New int grid of %s, X: %d, Y: %d, to be: \n\t %v \n instead got: \n\t %v",
				sg.str, sg.xSize, sg.ySize, sg.grid, ng)
		}
	}
}

type sampleSnake struct {
	snake Snake
	sg    *sampleGrid
}

func newSampleSnakes(t byte) []sampleSnake {
	var sampleSnakes []sampleSnake
	for i, sg := range sampleHGrids {
		sg := sg //Clone sg - so the pointer does not get overwritten
		ng := NewIntGrid(sg.str, sg.xSize, sg.ySize)
		var sn sampleSnake
		switch t {
		case 'h':
			sn = sampleSnake{Snake{ng, sg.sSize, make(chan []int), make(chan int)}, &sampleHGrids[i]}
		case 'v':
			sn = sampleSnake{Snake{ng, sg.sSize, make(chan []int), make(chan int)}, &sampleVGrids[i]}
		case 'd':
			sn = sampleSnake{Snake{ng, sg.sSize, make(chan []int), make(chan int)}, &sampleDGrids[i]}
		}
		sampleSnakes = append(sampleSnakes, sn)
	}
	return sampleSnakes
}

func TestScanHorizontal(t *testing.T) {
	sampleSnakes := newSampleSnakes('h')
	for _, s := range sampleSnakes {
		go s.snake.ScanHorizontal()
		var scanOut [][]int
		for scanned := range s.snake.ScanChan {
			scanOut = append(scanOut, scanned)
		}
		if !in.Unordered2DSliceEqual(scanOut, *s.sg.scanned) {
			t.Errorf("Expected scanning: \n\t%v \nto return: \n\t%v \ninstead got:\n\t%v",
				s.snake.Grid, *s.sg.scanned, scanOut)
		}
	}
}

func TestScanVertical(t *testing.T) {
	sampleSnakes := newSampleSnakes('v')
	for _, s := range sampleSnakes {
		go s.snake.ScanVertical()
		var scanOut [][]int
		for scanned := range s.snake.ScanChan {
			scanOut = append(scanOut, scanned)
		}
		if !in.Unordered2DSliceEqual(scanOut, *s.sg.scanned) {
			t.Errorf("Expected scanning: \n\t%v \nto return: \n\t%v \ninstead got:\n\t%v",
				s.snake.Grid, *s.sg.scanned, scanOut)
		}
	}
}

func TestScanDiagonal(t *testing.T) {
	sampleSnakes := newSampleSnakes('d')
	for _, s := range sampleSnakes {
		go s.snake.ScanDiagonal()
		var scanOut [][]int
		for scanned := range s.snake.ScanChan {
			scanOut = append(scanOut, scanned)
		}
		if !in.Unordered2DSliceEqual(scanOut, *s.sg.scanned) {
			t.Errorf("Expected scanning: \n\t%v \nto return: \n\t%v \ninstead got:\n\t%v",
				s.snake.Grid, *s.sg.scanned, scanOut)
		}
	}
}

func TestSum(t *testing.T) {
	sampleSnakes := newSampleSnakes('h')
	for i, s := range sampleSnakes {
		go func(i int, s *sampleSnake) {
			for _, row := range *s.sg.scanned {
				s.snake.ScanChan <- row
			}
			close(s.snake.ScanChan)
		}(i, &s)
		go s.snake.Sum()
		var summed []int
		for sum := range s.snake.SumChan {
			summed = append(summed, sum)
			if !in.IntInSlice(sum, *s.sg.summed) {
				t.Errorf("Expected sum %v to be in %v", sum, *s.sg.summed)
			}
		}
		if !in.UnorderedIntSliceEqual(summed, *s.sg.summed) {
			t.Errorf("Expected %v to be %v", summed, *s.sg.summed)
		}
	}
}

func TestMuxIntChans(t *testing.T) {
	cs := []chan int{make(chan int), make(chan int), make(chan int)}
	for i, row := range sampleSums {
		go func(i int, row []int) {
			for _, x := range row {
				cs[i] <- x
			}
			close(cs[i])
		}(i, row)
	}

	out := make(chan int)
	go MuxIntChans(cs, out)
	var muxed []int
	for x := range out {
		muxed = append(muxed, x)
	}

	var appended []int
	for _, row := range sampleSums {
		appended = append(appended, row...)
	}
	if !in.UnorderedIntSliceEqual(muxed, appended) {
		t.Errorf("Expected %v to be %v", muxed, appended)
	}
}

func TestMaxFromChan(t *testing.T) {
	var appended []int
	for _, row := range sampleSums {
		appended = append(appended, row...)
	}

	var m = make(chan int)
	go func() {
		for _, x := range appended {
			m <- x
		}
		close(m)
	}()

	ms := MaxFromChan(m)
	ss := appended[len(appended)-1]
	if ms != ss {
		t.Errorf("Expected %d to be %d", ms, ss)
	}
}
