package grid

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
)

type IntGrid [][]int

func stringToIntSlice(in string) []int {
	vals := strings.Fields(in)
	ints := make([]int, len(vals))
	for i, val := range vals {
		n, err := strconv.Atoi(val)
		if err != nil {
			panic("String does not represent an int")
		}
		ints[i] = n
	}
	return ints
}

func NewIntGrid(input string, xsize, ysize int) IntGrid {
	ig := make(IntGrid, ysize)
	ints := stringToIntSlice(input)
	for i := range ig {
		ig[i], ints = ints[:xsize], ints[xsize:]
	}
	return ig
}

type Snake struct {
	Grid     IntGrid
	Size     int
	ScanChan chan []int
	SumChan  chan int
}

func (snake *Snake) ScanHorizontal() {
	var group sync.WaitGroup
	for _, row := range snake.Grid {
		group.Add(1)
		go func(row []int) {
			if snake.Size > len(row) {
				//Panic - there is no reasonably graceful way to deal with this
				panic(fmt.Sprintf("The size of the snake is too long, the maximum is %d", len(row)))
			}
			for i := 0; (i + snake.Size) <= len(row); i++ {
				snake.ScanChan <- row[i : i+snake.Size]
			}
			group.Done()
		}(row) // So we create a new variable for the goroutine, instead of reasigning
	}
	group.Wait()
	close(snake.ScanChan)
}

func (snake *Snake) ScanVertical() {
	var group sync.WaitGroup
	// Todo need to check each row is of regular length
	for col := 0; col < len(snake.Grid[0]); col++ {
		group.Add(1)
		go func(col int) {
			for row := 0; (row + snake.Size) <= len(snake.Grid); row++ {
				var s []int
				for i := row; i < (row + snake.Size); i++ {
					s = append(s, snake.Grid[i][col])
				}
				snake.ScanChan <- s
			}
			group.Done()
		}(col)
	}
	group.Wait()
	close(snake.ScanChan)
}

func (snake *Snake) ScanDiagonal() {
	var group sync.WaitGroup
	for row := 0; (row + snake.Size) <= len(snake.Grid); row++ {
		group.Add(2)
		var rows [][]int
		for i := row; i < (row + snake.Size); i++ {
			rows = append(rows, snake.Grid[i])
		}
		go snake.ScanDiagonalForward(rows, &group)
		go snake.ScanDiagonalBackward(rows, &group)
	}
	group.Wait()
	close(snake.ScanChan)
}

func (snake *Snake) ScanDiagonalForward(rows [][]int, group *sync.WaitGroup) {
	for dia := 0; (dia + snake.Size) <= len(rows[0]); dia++ {
		var s []int
		r := 0
		for i := dia; i < (dia + snake.Size); i++ {
			s = append(s, rows[r][i])
			r += 1
		}
		snake.ScanChan <- s
		s = []int{}
	}
	group.Done()
}

func (snake *Snake) ScanDiagonalBackward(rows [][]int, group *sync.WaitGroup) {
	for dia := len(rows[0]) - 1; dia >= snake.Size; dia-- {
		var s []int
		r := 0
		for i := dia; i > (dia - snake.Size); i-- {
			s = append(s, rows[r][i])
			r += 1
		}
		snake.ScanChan <- s
		s = []int{}
	}
	group.Done()
}

func (snake *Snake) Sum() {
	for r := range snake.ScanChan {
		sum := 0
		for _, val := range r {
			sum += val
		}
		snake.SumChan <- sum
	}
	close(snake.SumChan)
}

func (snake *Snake) Product() {
	for r := range snake.ScanChan {
		sum := 0
		for _, val := range r {
			if sum == 0 {
				sum = val
			} else {
				sum *= val
			}
		}
		snake.SumChan <- sum
	}
	close(snake.SumChan)
}

func MuxIntChans(inputs []chan int, output chan<- int) {
	var group sync.WaitGroup
	for i := range inputs {
		group.Add(1)
		go func(input <-chan int) {
			for val := range input {
				output <- val
			}
			group.Done()
		}(inputs[i])
	}
	go func() {
		group.Wait()
		close(output)
	}()
}

func MaxFromChan(s <-chan int) int {
	max := 0
	for {
		n, ok := <-s
		fmt.Println(n)
		if !ok {
			return max
		}
		if n > max {
			max = n
		}
	}
}
